<#include "header.ftl">
	
	<div class="page-header">
		<h1>Blog</h1>
	</div>
	<#list posts as post>
  		<#if (post.status == "published")>
  			<p><a href="${post.uri}">${post.date?string("dd MMMM yyyy")} - ${post.title}</a></p> 
  		</#if>
  	</#list>
	
	<hr />
	
	<p>Older posts are available in the <a href="${content.rootpath}${config.archive_file}">archive</a>.</p>

<#include "footer.ftl">