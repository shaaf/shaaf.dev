</main>

  <footer class="pt-5 my-5 text-muted border-top">
    <p>
      <a href="#">Back to top</a>
    </p>
  </footer>
  <script src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>dist/js/bootstrap.bundle.min.js"></script>

  </body>
</html>