<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.83.1">
    <title>Blog Template · Bootstrap v5.0</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/blog/">
    
    
<!-- Bootstrap core CSS -->
<link href="<#if (content.rootpath)??>${content.rootpath}<#else></#if>dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900&amp;display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="blog.css" rel="stylesheet">
  </head>
  <body>
    
    <header class="p-3 mb-3 border-bottom">
        <div class="container">
          <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none">
                <img src="https://avatars.githubusercontent.com/u/474256?v=4" alt="mdo" width="32" height="32" class="rounded-circle">
            </a>
    
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
              <li><a href="#" class="nav-link px-2 link-secondary">Syed M Shaaf</a></li>
            </ul>
    
            <div class="text-end">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
              <a href="feed.xml" class="d-block link-dark text-decoration-none" aria-expanded="false">
                    <img src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>img/feed.svg" alt="mdo" width="32" height="32">
              </a>
              <a href="https://github.com/sshaaf" class="d-block link-dark text-decoration-none" aria-expanded="false">
                <img src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>img/github.svg" alt="mdo" width="32" height="32">
              </a>
              <a href="https://twitter.com/syshaaf" class="d-block link-dark text-decoration-none" aria-expanded="false">
                <img src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>img/twitter.svg" alt="mdo" width="32" height="32">
              </a>
              <a href="https://www.linkedin.com/in/shaaf/" class="d-block link-dark text-decoration-none" aria-expanded="false">
                <img src="<#if (content.rootpath)??>${content.rootpath}<#else></#if>img/linkedin.svg" alt="mdo" width="32" height="32">
              </a>
            </ul>
            </div>
          </div>
        </div>
      </header>

<main class="container">
